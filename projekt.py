import numpy as np
import string
import cv2
import time
import re
import os
from os.path import isfile, join
from matplotlib import pyplot as plt
from operator import attrgetter
import math
import anisotropic as an

#import denoise as dn
#import showall as sa
#import histeq as heq
import cpobasic as my

########################
## phase 1
## loading images
########################
#getting courrent path
print 'OpenCV verion running: ', cv2.__version__
print 'tested on 2.4.7'

path = os.getcwd()
print "searching for file to compute in: ", path

#getting all the files in directory
allfiles = [ f for f in os.listdir(path) if (isfile(join(path, f)) & f.endswith(".tiff")) ]
allfiles.sort(key=my.natural_keys)

#printing found files
print "found files: \n"
for instance in allfiles:
    print instance 

img         = []
img_color   = []
for testfile in allfiles:
    img.append(cv2.imread(testfile, 0))
    img_color.append(cv2.imread(testfile, -1))
    
k_small = cv2.getStructuringElement(cv2.cv.CV_SHAPE_ELLIPSE, (3,3))
k_medium = cv2.getStructuringElement(cv2.cv.CV_SHAPE_ELLIPSE, (17,17))

########################
## phase 2
## initial image 
## improvements
########################
for iteri in range(len(img)):
    i_color  = img_color[iteri]
    i= img[iteri] 
    #making images smaller
    i = cv2.pyrDown(i)
    h = i.shape[1] 
    w = i.shape[0]

    i_color = my.pyrDownColor(i_color)
    
    #normalisation
    max_value = 0;
    x =-1 # not used
    (_, max_value,_,_) = cv2.minMaxLoc(i)
    i = np.multiply(i , (float(255)/max_value))
    i = i.astype(np.uint8)
    base = np.copy(i)

    #some method 
    i = cv2.adaptiveThreshold(i,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,25,-5)
    th = np.copy(i)
    i = cv2.dilate(i,k_medium, anchor=(-1,-1), iterations=2)
    dil1 = np.copy(i)

    i = cv2.inpaint(base,dil1,3,cv2.INPAINT_TELEA)
    background = np.copy(i)
    i = cv2.absdiff(base, background)
    diff = np.copy(i)

    (_, i) = cv2.threshold(diff,10,255,cv2.THRESH_BINARY)
    i = cv2.dilate(i,k_small)
    dil2 = np.copy(i)

    (contours,_) = cv2.findContours(i,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(i,contours, -1,255,cv2.cv.CV_FILLED)
    contours1 = np.copy(i)

    i = cv2.inpaint(base, i, 3, cv2.INPAINT_TELEA)
    diff2 = cv2.absdiff(base, i)

    clahe= cv2.createCLAHE(clipLimit=4, tileGridSize=(9,9))
    diff2 = clahe.apply(diff2)
    diff2c = np.copy(diff2)

    ########################
    ## phase 3
    ## finding objects and  
    ## separating them
    ########################
    #enhancements
    diff2 = my.denoise(diff2)
    blurred = cv2.GaussianBlur(diff2,(5,5),7)

    #edge detections + contours computing
    binaryImg = cv2.Canny(blurred,50,90)

    kernel          = np.ones((5,5),np.uint8)
    closedImg       = cv2.morphologyEx(binaryImg,cv2.MORPH_CLOSE,kernel)

    (contours, hierarchy)   = cv2.findContours(closedImg,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
    (contours,bad_c)        = my.removeTrashContours(contours, w/10)

    blank = np.zeros_like(binaryImg)
    croppedImgs = []
    croppedDataArray=[]
    isGood = []
    moments = []
    details = [] # computed details of an object (cx,cy, type,  details ...)
    for contour in contours:
        # computing moments
        moments.append(cv2.moments(contour))

        # croping the image
        (croppedImg,data) = my.cropObjectFromImg(diff2c,contour,5)
        croppedDataArray.append(data)

        # creating mask
        hull = my.getHull(contour)
        tempBool = my.isOneObject(contour,factor=0.3,maxCount=5,minDefValue=9,maxDefCount=4)
        isGood.append(tempBool)
        #if not tempBool:
        mask = np.zeros_like(croppedImg)
        #cv2.polylines(mask,np.int32([hull]),True, 255,thickness=-1)
        cv2.drawContours(mask,[hull], -1 , 255, thickness=-1, offset=(-1*data[0], -1*data[1]))
        #applying mask
        bmask = cv2.threshold(mask,1,1,cv2.THRESH_BINARY)
        maskedCroppedImg = cv2.bitwise_and(croppedImg, mask)
        croppedImgs.append( maskedCroppedImg)

    
    # showing some results 
    for no in range(0,len(croppedDataArray)):
        color = 52
        if isGood[no]:
            color = 255
            #cv2.imshow('sth', croppedImgs[no])
            #cv2.waitKey(0)

        (x1,y1,w,h) = croppedDataArray[no]
        cv2.rectangle(diff2, (x1,y1),(x1+w,y1+h),color)

    # items recognition
    for no in range(0,len(croppedDataArray)):
        #approx = cv2.approxPolyDP(contours[no],0.09*cv2.arcLength(contours[no],True),True)
        approx = cv2.approxPolyDP(contours[no],1,True)
        cv2.drawContours(diff2,[approx],-1,255,thickness=3)

        # getting center of mass
        (cx,cy) = my.getCenterOfMass(moments[no])

        #writing some info
        cf = my.computeCircleFactor(contours[no])
        cf = my.walkaround(cf)

        #cf = my.computeCircleFactor(contour[no])
        cf = my.isSimilarToCircle(contours[no])
        if cf == 3 :
            #cv2.putText(diff2,str(cf),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,0.5,255)
            details.append((cx,cy,my.E_THINGS.LOC_P,2,2))
        elif cf == 1 :
            details.append((cx,cy,my.E_THINGS.NUT_P,2))
        elif cf == 0 :
            if isGood[no]:
                det = my.checkScrew(croppedImgs[no], contours[no], croppedDataArray[no][0], croppedDataArray[no][1])
                if det is not None and len(det) == 3:
                    length, dia, isFlat = det
                else:
                    details.append((cx,cy,my.E_THINGS.UNDEF))
                    continue

                if (length is not None) and ( dia is not None):
                    if length == -1:
                        details.append((cx,cy,my.E_THINGS.UNDEF))
                        continue

                    if isFlat:
                        details.append((cx,cy,my.E_THINGS.SCREW_FLAT_FILLISTER,length,dia))
                    else:
                        details.append((cx,cy,my.E_THINGS.SCREW_BUTTON,length,dia))

            
        #else:
        #cv2.putText(diff2,str(approx.shape[0]),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,1,255)
    for d in details:
        my.describeThing(i_color, d)



    #showing some output
    #cv2.imshow('-',diff2)
    cv2.imshow(str(iteri),i_color)
    cv2.waitKey(0)


