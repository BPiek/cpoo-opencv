from __future__ import division
import numpy as np
import string
import cv2
import time
import re
import os
from os.path import isfile, join
from matplotlib import pyplot as plt
from operator import attrgetter
import math
import anisotropic as an
import math

#zmniejszenie rozmiarow
def pyrDownArr(imgVect):
    ret = []

    for i in imgVect:
        ret.append(cv2.pyrDown(i))

    return ret

# oczyszczenie obrazu z szumow
def denoise(iimg):
    a = np.zeros_like(iimg)
    a = cv2.fastNlMeansDenoising(iimg,None,10,7,21)

    return a



#proste rozszerzenie histogramu (mnozenie)
def scaleLum(iimg):
        max_value = 0;
        (_, max_value,_,_) = cv2.minMaxLoc(iimg)

        ret = np.multiply(iimg , (float(255)/max_value))
        ret = ret.astype(np.uint8)
        
        return ret


def showAllImages(img_computed): 
    int_i = 1
    for i in img_computed:
        print int_i
        int_i+=1
        cv2.imshow(str(int_i), i)
        while(1):
            k = cv2.waitKey(1) & 0xFF
            if k == 27:
                break
    print 'closing . . .'
    cv2.destroyAllWindows()


def sharpenImage(img):
    #gauss  = cv2.GaussianBlur(img,(gsize,gsize),gsigmax)
    gauss  = cv2.GaussianBlur(img,(0,0),3)
    return cv2.addWeighted(img,1.5,gauss, -0.5,0)


def removeTrashContours(contours, minLength):
    good    = []
    bad     = []
    for c in contours:
        perimeter = cv2.arcLength(c, True)
        if perimeter < minLength :
            bad.append(c)
            continue

        rect = cv2.minAreaRect(c)

        good.append(c)
    return (good,bad)

#natural keys for sorting
def natural_keys(text):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', text)]

#get convex hull
def getHull(contour):
    hull = cv2.convexHull(contour,returnPoints=True)
    (count,_,_) = hull.shape
    hull.ravel()
    hull.shape = (count,2)
    return hull

#draw hull
def drawHull(hull,img):
    cv2.polylines(img,np.int32([hull]),True,255)

def cropObjectFromImg(img,contour,padding):
    a = cv2.boundingRect(contour)
    croppedImg =  img[ a[1]-padding:a[1]+a[3]+padding
                     , a[0]-padding:a[0]+a[2]+padding]

    #x,y,w,h
    croppedData = (a[0]-padding,a[1]-padding,a[2]+2*padding, a[3]+2*padding)
    return (croppedImg, croppedData)
 
def isOneObject(contour, factor=1/8.0,maxCount=3, minDefValue=0, maxDefCount=5):
    #computing hull and defects
    hull = cv2.convexHull(contour,returnPoints=False)
    defects = cv2.convexityDefects(contour,hull)
    defects_val = defects[ :-1 ,0, 3]
    defects_val = np.sort(defects_val)[::-1]/256
    
    #computing min area bounding rect and the walls
    rect = cv2.minAreaRect(contour)
    box = cv2.cv.BoxPoints(rect)
    box = np.int0(box)
    dist1 = np.linalg.norm(box[0]-box[1])
    dist2 = np.linalg.norm(box[1]-box[2])
    maxDist = max((dist1,dist2))

    #checking contours
    def_sum     = 0     # sum of defects distances (sum of max <maxCount> elements)
    def_count   = 0     # count of distances > minDefValue
    for defect in defects_val:
        if defect > minDefValue:
            def_count += 1 
        else:
            continue

        if maxCount == 0 :
            continue

        def_sum += defect
        maxCount -= 1

    #printing some debug
    #if not((def_sum < maxDist*factor) and (def_count<maxDefCount)) :
        #print '=============='
        #print 'defects_val: ', defects_val, ' def_sum: ', def_sum, ' def count: ' , def_count, ' maxDist*factor = ', maxDist, ' * ' , factor, ' = ' , maxDist*factor
    return ((def_sum < maxDist*factor) and (def_count<maxDefCount)) 

def createMask(img, contour, padding=0):
    mask = np.zeros_like(img)
    cv2.drawContours(mask,[contour],-1,1,thickness=-1)
    bmask = cv2.threshold(mask,1,1,cv2.THRESH_BINARY)
    if(padding>0):
        kernel = np.ones((3,3),np.uint8)
        bmask = cv2.dilate(mask,kernel,iterations=padding)
    return bmask

def getCenterOfMass(moments):
    cx = int(moments['m10']/moments['m00'])
    cy = int(moments['m01']/moments['m00'])
    return(cx,cy)

# returns:
#   0 - not a circle-like shape
#   1 - circle-like shape (NUT)
#   2 - circle-like shape (NUT/LOCKWASHER)
#   3 - circle
def isSimilarToCircle(contour, factor=None):
    (_,radius) = cv2.minEnclosingCircle(contour)
    circle_area = math.pi*radius**2

    contour_area = cv2.contourArea(contour)
    factor = contour_area/circle_area

    if factor > 0.87:
        return 3
    elif factor > 0.82:
        return 2
    elif factor > 0.65:
        return 1
    else:
        return 0

def computeCircleFactor(contour):
    (_,radius) = cv2.minEnclosingCircle(contour)
    circle_area = math.pi*radius**2

    contour_area = cv2.contourArea(contour)
    factor = float( contour_area/circle_area)
    return factor

def walkaround(x):
    if x > 0.9 : return 0.9
    if x > 0.85 : return 0.85
    if x > 0.8 : return 0.8
    if x > 0.75 : return 0.75
    if x > 0.7 : return 0.7
    return x

def pyrDownColor(colorImg):
    b = colorImg[:,:,0]
    b = cv2.pyrDown(b)

    g = colorImg[:,:,1]
    g = cv2.pyrDown(g)

    r = colorImg[:,:,2]
    r = cv2.pyrDown(r)

    colorImg = np.dstack((b,g,r))

    return colorImg

def enum(**enums):
    return type('Enum', (), enums)

E_THINGS = enum(
          SCREW_BUTTON = 's. button'
        , SCREW_FLAT_FILLISTER = 's. filister'      ## (.. , )
        , NUT_P = 'nut'                             ## (.. , r_inner)
        , LOC_P = 'loc'                             ## (.. , r_big, r_small )
        , LOC_SPLIT = ' lockwasher (split)'
        , UNDEF = '?'
        )

def describeUndef(img_color, cx,cy):
    description = E_THINGS.UNDEF  
    cv2.putText(img_color,str(description),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255))

def describeLOC_P(img_color,cx, cy, r_big, r_small):
    description = E_THINGS.LOC_P + ' ' + str(r_small) + '(' + str(r_big) + ')'
    cv2.putText(img_color,str(description),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,0.5,(255,0,0))

def describeNUT_P(img_color,cx, cy, r_inner):
    description = E_THINGS.NUT_P + ' ' + str(r_inner) 
    cv2.putText(img_color,str(description),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,0))

def describeSCREW_BUTTON(img_color, cx,cy,lenght=0,diameter=0):
    description = E_THINGS.SCREW_BUTTON + ' ' +"len: " + "{:3.1f}".format(lenght) +"d: " + "{:3.1f}".format(diameter)
    cv2.putText(img_color,str(description),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0x91,0x5D,0x77))

def describeSCREW_FLAT(img_color, cx,cy,lenght=0,diameter=0):
    description = E_THINGS.SCREW_FLAT_FILLISTER + ' ' +"len: " + "{:3.1f}".format(lenght) +"d: " + "{:3.1f}".format(diameter)
    cv2.putText(img_color,str(description),(cx,cy),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0x6C,0x87,0x7A))
def describeThing(img_color,details):
    thingType = details[2]
    if thingType == E_THINGS.UNDEF:
        describeUndef(img_color,details[0], details[1])
    elif thingType == E_THINGS.LOC_P:
        describeLOC_P(img_color,details[0], details[1],details[3], details[4])
    elif thingType == E_THINGS.NUT_P:
        describeNUT_P(img_color,details[0], details[1],details[3])
    elif thingType == E_THINGS.SCREW_BUTTON:
        describeSCREW_BUTTON(img_color,details[0],details[1],details[3], details[4])
    elif thingType == E_THINGS.SCREW_FLAT_FILLISTER:
        describeSCREW_FLAT(img_color,details[0],details[1],details[3], details[4])
def getVector( p1, p2):
    return (p2[0] - p1[0], p2[1] - p1[1])

def getLine(vx,vy,x,y,cols,rows):
    x0 = x1 =  y0 = y1 = 0
    if abs(vx) > abs(vy):
        move = -x/vx
        y0=move*vy + y

        x1 = cols
        move2 = (cols - x)/vx
        y1 = move2*vy + y 
    else:
        move = -y/vy
        x0 = move*vx + x

        y1 = rows
        move2 = (rows - y)/vy
        x1 = move2*vx + x
    return ((int(x0),int(y0)), (int(x1),int(y1)))

def drawLine(img,vx,vy,x,y, thickness=1):
    (rows,cols) = img.shape[:2]
    (p1,p2) = getLine(vx,vy,x,y,cols,rows)
    cv2.line(img,p1, p2, 1,thickness=thickness)

def drawPerpLine(img,vx,vy,x,y):
    vx2 = -1*vy
    vy2 = vx
    drawLine(img,vx2,vy2,x,y)

def drawLine2p(img, x1,y1,x2,y2, thickness = 1):
    vx = x2-x1
    vy = y2-y1

    drawLine(img,vx,vy,x1,y1, thickness = thickness)

def drawPerpLines2p(img, x1, y1, x2, y2):
    vx = x2-x1
    vy = y2-y1
    drawPerpLine(img,vx,vy,x1,y1)

def filterPoints(points):
    ret = []
    iter = 0
    if len(points) >= 2:
        minp = points[0]
        maxp = points[0]
        for p in points:
            if len(p) != 2:
                print 'err fp'
                continue
            iter+=1
            if p[0] < minp[0]:
                minp = p
            elif p[0] > maxp[0]:
                maxp = p
        if iter >= 2:
            ret.append(maxp)
            ret.append(minp)
            return ret
    return [[-1,-1]]
def pdist(a,b):
    return np.linalg.norm(np.array(a) -np.array(b))
def pldist(a,b,x,y):
    return abs(a*x-y+b)/float(math.sqrt(a*a+1))
def filterPoints2(points, maxDiff = 6):
    #print 'OOOOOOOOOOOOOOOOOOOO'
    #print points
    #print 'OOOOOOOOOOOOOOOOOOOO'
    if points is None or len(points) < 2:
        print 'fp2: not enough lines'
        return
    variation = []
    for i in range(len(points)-1):
        for j in range(i+1,len(points)):
            variation.append([points[i],points[j]])

    dist = []
    for p1,p2 in variation:
        dist.append(pdist(p1,p2))
    minVal = max(dist) - maxDiff

    okDists = []
    for d in dist:
        if d >= minVal:
            okDists.append(d)
        
    avg = sum(okDists)/len(okDists)

    diffs = []
    for o in okDists:
        diffs.append( abs(o-avg))

    ids = np.array(diffs).argsort()[-1:]

    foundDist = okDists[ids[0]]
    idx = dist.index(foundDist)
    
    return variation[idx]


 


def getLength(contoursImg, lineImg):
    intersection =cv2.bitwise_and(contoursImg,lineImg )
    #intersection2 =cv2.bitwise_or(contoursImg,lineImg )
    #myShow(intersection,description="intersect", color=1, blankImg=False, waiting=False)
    #myShow(intersection2,description="intersect2", color=1, blankImg=False, waiting=False)
    pixelpoints = cv2.findNonZero(intersection)
    points = []
    if pixelpoints is None:
        return -1
    for i in pixelpoints:
        for j in i:
            points.append([j[0], j[1]])
    points = filterPoints2(points)
    if( points is None) or len(points) < 2:
        return -1
    if len(points) > 2:
        print 'warning gl: taken only 2 points'

    return np.linalg.norm(np.array(points[0]) -np.array( points[1]))

def getTrend(l,r, sameMaxFactor = 0.9):
    temp = l/r
    if abs(temp) >= sameMaxFactor:
        return 0
    if temp < 1:
        return 1
    return -1

def getLineParameters(p1, p2):
    a = (p2[1] - p1[1]) / (p2[0] - p1[0])
    b = p1[1] - a * p1[0]
    return (a,b)

def isOnLeft(p1, p2, vx, vy):
    if vx == 0:
        return p1[0] < p1[0]

    m = (p2[1] - p1[1])/float(vy)
    x = p1[0] + m * vx

    return x < p2[0]

def turn(vx, vy, toLeft = True):
    ret = [vy,vx]

    if toLeft:
        ret[0] *= -1
    else:
        ret[1] *= -1

    return ret

def normalizeVect((vx,vy)):
    return  np.array([vx,vy]) /  np.linalg.norm([vx,vy])

def areParallel(l1_p1, l1_p2, l2_p1, l2_p2):
    parallel_max_factor = 0.2
    # case when cant compute one or more line parameters (eg x=5, a->oo) 
    if l1_p1[0] == l1_p2[0] and l2_p1[0] == l2_p2[0] :
        return True
    if l1_p1[0] == l1_p2[0]:
        (a,_) = getLineParameters(l2_p1, l2_p2)
        if abs(a) > 10:
            return True
        return False
    if l2_p1[0] == l2_p2[0] :
        (a,_) = getLineParameters(l1_p1, l1_p2)
        if abs(a) > 10:
            return True
        return False

    #std case

    (l1_a, l1_b) = getLineParameters(l1_p1, l1_p2)
    (l2_a, l2_b) = getLineParameters(l2_p1, l2_p2)

    alpha1 = np.arctan(l1_a)
    alpha2 = np.arctan(l2_a)
    alphadiff = abs(alpha1-alpha2)
    return alphadiff <= parallel_max_factor

    
    if abs(2*(l1_a - l2_a)/(l1_a + l2_a)) > parallel_max_factor :
        return False
    return True


def getOffsetLine(l1_p1, l1_p2, l2_p1, l2_p2, distance):
    # lines are parallel
    #parallel_max_factor = 0.2

    if l1_p1[0] == l1_p2[0] or l2_p1[0] == l2_p2[0] :
        direction = 1
        if l1_p1[0] < l2_p1[0] : 
            direction = -1
        
        x = l1_p1[0] + direction*distance
        return [ [x,l1_p1[1]] , [x , l1_p2[1]] ]


    (l1_a, l1_b) = getLineParameters(l1_p1, l1_p2)
    (l2_a, l2_b) = getLineParameters(l2_p1, l2_p2)

    #if abs(2*(l1_a - l2_a)/(l1_a + l2_a)) > parallel_max_factor :
    if not areParallel(l1_p1, l1_p2, l2_p1, l2_p2):
        print 'lines not parallel! ' 
        return None

    (l1_vx, l1_vy) = getVector(l1_p1, l1_p2) 
    #(l2_vx, l2_vy) = getVector(l2_p1, l2_p2)

    onLeft = isOnLeft(l1_p1, l2_p1, l1_vx, l1_vy)
    #getting perp. vect
    vect = turn(l1_vx, l1_vy,onLeft)

    p1 = l1_p1 + normalizeVect(vect)*distance
    p2 = p1 + np.array([l1_vx,l1_vy])

    return [[p1[0], p1[1]], [p2[0],p2[1]]]

def getAvgDist(l1_p1, l1_p2, l2_p1, l2_p2):
    l1_p1 = np.array(l1_p1)
    l1_p2 = np.array(l1_p2)
    l2_p1 = np.array(l2_p1)
    l2_p2 = np.array(l2_p2)
    d1 =  np.linalg.norm(l1_p1 - l2_p1) + np.linalg.norm(l1_p2 - l2_p2)
    d2 =  np.linalg.norm(l1_p1 - l2_p2) + np.linalg.norm(l1_p2 - l2_p1)
    return min(d1,d2)

def w():
    cv2.waitKey(0)

def getScrewLines(lines, minDist=19):
    #print 'getScrewLines <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
    a,b,c = lines.shape
    ll=[]
    goodLines = []
    if range(b) > 2:
        #print '<<<<<<<<<<<<<<1, line count: ' , b
        ##########
        ##debug
        #cv2.namedWindow('center', cv2.WINDOW_NORMAL)
        #cv2.resizeWindow('center', 700, 700)
        #cv2.namedWindow('a', cv2.WINDOW_NORMAL)
        #cv2.resizeWindow('a', 700, 700)
        #print lines
        #iii = np.zeros_like(contoursImg)
        #for x1,y1,x2,y2 in lines[0]:
            #cv2.line(iii,(x1,y1),(x2,y2),255,thickness = 1)
#
        #contoursImg *=255
        #cv2.imshow('center', contoursImg)
        #cv2.imshow('a', iii)
        #cv2.waitKey(0)
#
        ##########

        # finding longest 
        for i in range(b):
            ll.append(np.linalg.norm(np.array([lines[0][i][0], lines[0][i][1]]) -np.array( [lines[0][i][2], lines[0][i][3]])))
        ll = np.array(ll)
        linesToCheckCount = min(4, b) 
        indices = ll.argsort()[(-1*linesToCheckCount):][::-1]
        #print ' len: ' , len(indices), ' lines to check: ', linesToCheckCount, ' all lines count: ', b, ' ll count: ', len(ll)

        # finding distant pair that is parallel and long enough ( based on ranking )
        found = False
        for i in range(len(indices)-1):
            for j in range(i+1, len(indices)):
                #print 'checking :', i , " and ", j
                tl1p1 = [lines[0][indices[i]][0], lines[0][indices[i]][1]]
                tl1p2 = [lines[0][indices[i]][2], lines[0][indices[i]][3]]
                tl2p1 = [lines[0][indices[j]][0], lines[0][indices[j]][1]]
                tl2p2 = [lines[0][indices[j]][2], lines[0][indices[j]][3]]
                if getAvgDist(tl1p1,tl1p2, tl2p1, tl2p2) > minDist and areParallel(tl1p1, tl1p2, tl2p1, tl2p2):
                    #print '++good'
                    goodLines.append([tl1p1,tl1p2])
                    goodLines.append([tl2p1,tl2p2])
                    found = True
                else:
                    if not getAvgDist(tl1p1,tl1p2, tl2p1, tl2p2) > minDist :
                        print '--not distant!'
                    if not areParallel(tl1p1, tl1p2, tl2p1, tl2p2):
                        print '--found lines are not parallel'
                if found:
                    break
            if found:
                break
        #goodLines.append([[lines[0][indices[0]][0],lines[0][indices[0]][1]],[lines[0][indices[0]][2], lines[0][indices[0]][3]]])
        #goodLines.append([[lines[0][indices[1]][0],lines[0][indices[1]][1]],[lines[0][indices[1]][2], lines[0][indices[1]][3]]])
    else:
        print '<<<<<<<<<<<<<<2, line count: ' , b

        tl1p1 = [lines[0][0][0], lines[0][0][1]]
        tl1p2 = [lines[0][0][2], lines[0][0][3]]
        tl2p1 = [lines[0][1][0], lines[0][1][1]]
        tl2p2 = [lines[0][1][2], lines[0][1][3]]
        if areParallel(tl1p1, tl1p2, tl2p1, tl2p2):                    
            #print 'got 2 proper lines' 
            goodLines.append([tl1p1,tl1p2])
            goodLines.append([tl2p1,tl2p2])
        else :
            print 'got 2 lines but those lines are not parallel!'
    return goodLines

def myShow(img, contours=[],lines=[], description="x",color = 255, blankImg=True, waiting=True, contoursOffset = (0,0), retOnly=False, lthickness=1):
    if blankImg:
        img2 = np.zeros_like(img)
    else:
        img2 = np.copy(img)

    cv2.drawContours(img2,contours,-1,1,thickness = 1, offset=contoursOffset)
    if lines is not None:
        for [p1, p2] in lines:
           drawLine2p(img2,p1[0],p1[1],p2[0],p2[1],thickness=lthickness) 

    if color > 1:
        img2 *= color

    if retOnly:
        return img2
    cv2.namedWindow(description, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(description, 700, 700)
    cv2.imshow(description,img2)
    if waiting:
        cv2.waitKey(0)
    return img2


def checkScrew(croppedImg, contour, dx, dy, partNo = 30):
    print 'check screw ............................'
    # copy (test)
    mImg = np.copy(croppedImg)
    mContour = np.copy(contour)

    #creating blank img
    blank = np.zeros_like(croppedImg)
    blank2 = np.copy(blank)

    # finding a fitting line and drawing it on "blank" image
    #print croppedImg.shape
    [vx,vy,x,y] = cv2.fitLine(contour, cv2.cv.CV_DIST_L2,0,0.001,0.001)

    ##applying offset
    xx = x - dx
    yy = y - dy

    drawLine(blank,vx,vy,xx,yy)

    # drawing contours
    cv2.drawContours(blank2,[contour], -1 , 1, thickness=1, offset=(-1*dx, -1*dy))
    contoursImg = np.copy(blank2)
    
    ## finding intersection points
    #intersection =cv2.bitwise_and(blank, blank2)
    #pixelpoints = cv2.findNonZero(intersection)
#
    #perpLines = np.zeros_like(croppedImg)
    #print '...'
    #if pixelpoints is None :
        #print 'deb A'
        #return E_THINGS.UNDEF
#
    #points = []
    #for i in pixelpoints:
        #for j in i:
            #points.append([j[0], j[1]])
#
    ## find distant points 
    #points = filterPoints(points)
    #if len(points) != 2:
        #print 'deb B ', len(points), ' : ', points
        #return E_THINGS.UNDEF
    #for p in points:
        #drawPerpLine(perpLines, vx,vy,p[0], p[1])
         #
    #totalLength = np.linalg.norm(np.array(points[0]) -np.array(points[1]))
    #n = (points[1][0] - points[0][0])/float(vx)
    #n /=float(partNo)
    #print 'n= ' , n
#
    #widthVect = []
    #for i in range(partNo):
        #oldx = points[0][0]
        #oldy = points[0][1]
#
        #newx = oldx + n * vx * i
        #newy = oldy + n * vy * i
        #lineImg = np.zeros_like(blank2)
#
        #drawPerpLine(lineImg,vx,vy,newx,newy)
#
        #tempWidth = getLength(contoursImg,lineImg)
        #if tempWidth > 0:
            #widthVect.append(tempWidth)
#
    #if len(widthVect) < 3:
        #print 'deb C'
        #return E_THINGS.UNDEF
#
#
    #maxIdx = widthVect.index(max(widthVect))
    #l = True
    #if maxIdx >= len(widthVect)/2:
        #l = False
###################################################
    ## minAreaRect
    #rect = cv2.minAreaRect(contour)
    #box = cv2.cv.BoxPoints(rect)
    #box = np.int0(box)
    #box -= [dx,dy]
    #dist1 = np.linalg.norm(box[0]-box[1])
    #dist2 = np.linalg.norm(box[1]-box[2])
    #dist1Longer = dist1 > dist2
#
    #if dist1Longer:
        #tvx=box[1][0]-box[0][0]
        #tvy=box[1][1]-box[0][1]
        #tx =( box[1][0] + box[2][0] ) / 2
        #ty =( box[1][1] + box[2][1] ) / 2
    #else:
        #tvx=box[2][0]-box[1][0]
        #tvy=box[2][1]-box[1][1]
        #tx =( box[0][0] + box[1][0] ) / 2
        #ty =( box[0][1] + box[1][1] ) / 2
#
    #drawLine(contoursImg,tvx,tvy,tx,ty)
    #cv2.drawContours(contoursImg,[box],-1,255,2)
    #contoursImg *=255
    #cv2.imshow('aaaaaa',contoursImg)
    #cv2.waitKey(0)


###################################################
    ## hough
    blurred = cv2.GaussianBlur(mImg,(5,5),11)
    binaryImg = cv2.Canny(blurred,50,90)    

    lines = cv2.HoughLinesP(binaryImg, 1, math.pi/180.0, 1, np.array([]), minLineLength=30, maxLineGap=19)
    if lines is None:
        print 'HoughLines found none lines'
        #debug
        cv2.imshow('1', binaryImg)
        cv2.imshow('2', contoursImg)
        cv2.waitKey(0)
        return -1,-1,False
    #a,b,c = lines.shape
#
    #ll=[]
    #goodLines = []
    #if range(b) > 2:
        ##########
        ##debug
        #cv2.namedWindow('center', cv2.WINDOW_NORMAL)
        #cv2.resizeWindow('center', 700, 700)
        #cv2.namedWindow('a', cv2.WINDOW_NORMAL)
        #cv2.resizeWindow('a', 700, 700)
        #print lines
        #iii = np.zeros_like(contoursImg)
        #for x1,y1,x2,y2 in lines[0]:
            #cv2.line(iii,(x1,y1),(x2,y2),255,thickness = 1)
#
        #contoursImg *=255
        #cv2.imshow('center', contoursImg)
        #cv2.imshow('a', iii)
        #cv2.waitKey(0)
#
        ##########
        ## finding longest pair
        #for i in range(b):
            #ll.append(np.linalg.norm(np.array([lines[0][i][0], lines[0][i][1]]) -np.array( [lines[0][i][2], lines[0][i][3]])))
        #ll = np.array(ll)
        #indices = ll.argsort()[-2:][::-1]
        #goodLines.append([[lines[0][indices[0]][0],lines[0][indices[0]][1]],[lines[0][indices[0]][2], lines[0][indices[0]][3]]])
        #goodLines.append([[lines[0][indices[1]][0],lines[0][indices[1]][1]],[lines[0][indices[1]][2], lines[0][indices[1]][3]]])
    #else:
        #goodLines.append([[lines[0][0][0],lines[0][0][1]], [lines[0][0][2], lines[0][0][3]]])
        #goodLines.append([[lines[0][1][0],lines[0][1][1]],[lines[0][1][2], lines[0][1][3]]])
    goodLines = getScrewLines(lines)

    # computing center line
    if len(goodLines) != 2 :
        print 'proper lines not found! len: ', len(goodLines), ', shape: ' ,np.array( goodLines).shape 
        return -1,-1,False

    #myShow(croppedImg, [contour], goodLines, description = "znalezione linie",contoursOffset = (-dx,-dy))
    vx1 = goodLines[0][0][0] - goodLines[0][1][0]
    vx2 = goodLines[1][0][0] - goodLines[1][1][0]

    vy1 = goodLines[0][0][1] - goodLines[0][1][1]
    vy2 = goodLines[1][0][1] - goodLines[1][1][1]

    l2_p1 = 1 
    l2_p2 = 0
    #sameDirection = vx1/vx2 > 0
    sameDirection = (vx1 >= 0 and vx2>=0) or (vx1<0 and vx2<0)
    if sameDirection:
        l2_p1 = 0
        l2_p2 = 1


    line1_p1 = np.array( goodLines[0][0])
    line1_p2 = np.array( goodLines[0][1])
    line2_p1 = np.array( goodLines[1][l2_p1])
    line2_p2 = np.array( goodLines[1][l2_p2])

    centerLine_p1 = (line1_p1 + line2_p1) / 2 
    centerLine_p2 = (line1_p2 + line2_p2) / 2
    
    centerLine_p1 = centerLine_p1.astype(int)
    centerLine_p2 = centerLine_p2.astype(int)

    # intersect center line with contours
    centerLineImg = np.zeros_like(croppedImg)
    drawLine2p(centerLineImg,centerLine_p1[0], centerLine_p1[1], centerLine_p2[0],centerLine_p2[1],thickness=2)

################
    #window_width = window_height = 500
    #cv2.namedWindow('center', cv2.WINDOW_NORMAL)
    #cv2.resizeWindow('center', window_width, window_height)
    #cv2.namedWindow('contours', cv2.WINDOW_NORMAL)
    #cv2.resizeWindow('contours', window_width, window_height)
#
    ##centerLineImg *=255
    ##contoursImg *= 255
#
    #added = cv2.bitwise_or(contoursImg, centerLineImg)
    #added *=255
    #cv2.imshow('center', added)
    ##cv2.imshow('center', centerLineImg)
    ##contoursImg *= 255
    ##cv2.imshow('contours', contoursImg)
    #cv2.waitKey(0)
################
    intersection =cv2.bitwise_and(contoursImg, centerLineImg)
    testimg = cv2.bitwise_or(contoursImg,centerLineImg)
    #myShow(intersection,waiting=False,description="and", blankImg=False)
    #myShow(testimg,description="or", blankImg=False)
    #cv2.imshow('and', testimg*255)
    #cv2.imshow('or',intersection*255) 
    #w()
    pixelpoints = cv2.findNonZero(intersection)

    # filtering intersect points (finding distant points)
    if pixelpoints is None or len(pixelpoints) <2:
        if pixelpoints is not None:
            print 'deb A: ' , pixelpoints
        else:
            print 'deb A, None'
        return (-1,-1,False)

    points = []
    for i in pixelpoints:
        for (x,y) in i:
            points.append([x,y])
            #points.append([i[0], i[1]])

    #points = filterPoints(points)
    points = filterPoints2(points)
    if points is None or len(points) != 2:
        print 'deb B '#, len(points), ' : ', points
        return (-1,-1,False)

    #myShow(testimg,[contour],[points],"filtered center line",contoursOffset = (-dx,-dy))
    # computing length of a screw
    totalLength = np.linalg.norm(np.array(points[0]) -np.array(points[1]))

    if line1_p1[1] != line1_p2[1]:
        (a,b) = getLineParameters(line1_p1, line1_p2)
        xxx = line2_p1[0]
        yyy = line2_p1[1]
    else :
        (a,b) = getLineParameters(line2_p1, line2_p2)
        xxx = line1_p1[0]
        yyy = line1_p1[1]
    dia = pldist(a,b,xxx,yyy)
    #print line1_p1
    #print line1_p2
    #print line2_p1
    #print line2_p2
    
     
    start = False
    firstHeadW = 0
    iters = 0
    sumW = 0
    lenVect = []
    for i in range(0, 500, 1):
        offLine = getOffsetLine(line1_p1, line1_p2, line2_p1, line2_p2, i) 
        tempLineImg = myShow(contoursImg,lines=[offLine], retOnly=True, lthickness=1)
        #myShow(contoursImg,[mContour],[offLine],"offset line",contoursOffset = (-dx,-dy),lthickness=2)
        length = getLength(contoursImg*255,tempLineImg)
        if length <=0:
            tempLineImg = myShow(contoursImg,lines=[offLine], retOnly=True, lthickness=2)
            length = getLength(contoursImg*255,tempLineImg)

        #print 'len: ', length
        
        if length < totalLength/3 and length > 0 and not start:
            print 'start'
            start = True
            continue
        elif length <=0:
            sumFull = iters*firstHeadW
            if not start :
                print 'err, length <= 0 and not started'
                return (-1,-1,False)
            elif sumFull==0:
                print 'err, length <= 0 and sumFull == 0'
                return (-1,-1,False)
            factor = sumW/float(sumFull)
            lenVect3 = lenVect[-1*  min(3,len(lenVect)):]
            var =np.var( np.array(lenVect))
            var3 =np.var( np.array(lenVect3))
            std = np.std( np.array(lenVect))
            med = np.median( np.array(lenVect))
            avg = np.average( np.array(lenVect3))
            slength = totalLength - firstHeadW
            print '>>> var: ' ,var , ' var3: ',var3 , ' factor: ', factor, ' std: ',std , ' med ',med , ' avg ', avg
            isFlat = False
            if var < 2.5 and factor > 0.65 and std > 0.9:
                isFlat = True
            if var < 3 and avg > 7:
                isFlat = True

            #return factor
            #return var
            return slength, dia, isFlat
            #return avg
            #return med

        if start and length >= totalLength/3:
            print 'reset!' 
            start=False
            iters = 0
            sumW = 0
            firstHeadW = 0
            lenVect=[]
            continue
        
        if start:
            if firstHeadW == 0:
                print 'first: ', length
                firstHeadW = length
                continue
            if length <1.4:
                continue
            lenVect.append(length)
            iters += 1
            sumW += length
            #print iters, ": ", length, " (sum : ", sumW, " ), first * iter = ", iters * firstHeadW

        

    #offLine = getOffsetLine(line1_p1, line1_p2, line2_p1, line2_p2, 5) 
    #print 'offset line: ',  offLine
    #myShow(testimg,[mContour],[offLine],"offset line",contoursOffset = (-dx,-dy))
    #drawLine2p(croppedImg,p1[0], p1[1], p2[0], p2[1])
    #cv2.imshow('test', croppedImg)
    #cv2.waitKey(0)

    






    #cv2.line(centerLineImg, (centerLine_p1[0], centerLine_p1[1]), (centerLine_p2[0],centerLine_p2[1]),255,1)

    #for i in range(b):
    #    cv2.line(mImg, (lines[0][i][0], lines[0][i][1]), (lines[0][i][2], lines[0][i][3]), 255, 3 )
    
    #centerLineImg *=255
    #cv2.imshow('b', centerLineImg)
    #cv2.waitKey(0)


