import numpy as np
import string
import cv2
import time
import re
import os
from os.path import isfile, join
from matplotlib import pyplot as plt
from operator import attrgetter
import math
import anisotropic as an

#import denoise as dn
#import showall as sa
#import histeq as heq
import cpobasic as my

cv2.setUseOptimized(True)
def make4img(resize, *imgs):
    imgs_ = []
    newimgs = []
    i = 0
    for img in imgs:
        imgs_.append(cv2.resize(img, (0,0), fx=resize, fy=resize))
        newimgs.append(np.zeros_like(imgs_[i]))
    w =2* imgs_[0].shape[1] 
    h = 2* imgs_[0].shape[0]

    ret = np.zeros((h,w,1),np.uint8)
    ret[:h/2, :w/2,0] = imgs_[0]
    ret[:h/2, w/2:,0] = imgs_[1]
    ret[h/2:, :w/2,0] = imgs_[2]
    ret[h/2:, w/2:,0] = imgs_[3]

    return ret

def natural_keys(text):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', text)]

def prt_hist(img):
    hist = np.bincount(img.ravel(),minlength=256)
    plt.plot(hist);
    plt.xlim([0,256])
    plt.show() 


#test podstawowych metod
def test1(allfiles):
    i = 0;
    for image_name in allfiles:
        i+=1
        if(i<11):
            continue
          
        # creating window
        cv2.namedWindow(image_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(image_name, 1024,768)

        # load an color image in grayscale
        img = cv2.imread(image_name,0)
        img_color = cv2.imread(image_name,-1)

        img_color = cv2.pyrDown(img_color)

        #hsv
        hsv = cv2.cvtColor(img_color, cv2.COLOR_BGR2HSV)
        #hsv[:,:,0] = cv2.GaussianBlur(hsv[:,:,0],(5,5),0)
        ###
        #some tests
        #cv2.equalizeHist(img,img)

        max_value = 0;
        x =-1 # not used
        (_, max_value,_,_) = cv2.minMaxLoc(img)
        img = np.multiply(img , (float(255)/max_value))
        img = img.astype(np.uint8)
        #base = np.copy(i)

        img = cv2.bilateralFilter(img,10,50,120)
        
        clahe= cv2.createCLAHE(clipLimit=4, tileGridSize=(9,9))
        img = clahe.apply(img)

        #img = cv2.GaussianBlur(img,(5,5),5)
        #thres, img= cv2.threshold(img,19,255,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 61,9)
        #img = cv2.adaptiveBilateralFilter(img,(9,9),5)
        #img = cv2.GaussianBlur(img,(5,5),8)
        #img = cv2.Canny(img,130,200)

        #plotting edges
        #plt.subplot(121),plt.imshow(img,cmap = 'gray')
        #plt.title('Original Image'), plt.xticks([]), plt.yticks([])
        #plt.subplot(122),plt.imshow(edges,cmap = 'gray')
        #plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
        #plt.show()
        cv2.imshow(image_name, img)
        cv2.waitKey(0)
        cv2.destroyWindow(image_name)

        #for testing purse- dont need to loop thorugh all the pictures
        #brake 
def nothing():
    pass
#test trackbarow
def test2(allfiles):
    print "entering test2" 
    filename = allfiles[0]
    print "[test2] opening image: ", filename

    img = cv2.imread(filename, 0)
    cv2.namedWindow(filename, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(filename, 1024, 768)

    #creating trackbars
    print "creating trackbars"
    cv2.createTrackbar('d',filename, 10, 50, nothing)
    cv2.createTrackbar('sigmaColor',filename, 50, 255, nothing)
    cv2.createTrackbar('sigmaSpace',filename, 90, 90, nothing)
    cv2.imshow(filename,img)

    while(1):
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

        a = cv2.getTrackbarPos('d',filename)
        b = cv2.getTrackbarPos('sigmaColor',filename)
        c = cv2.getTrackbarPos('sigmaSpace',filename)

        img2 = cv2.bilateralFilter(img,a,b,c)

        cv2.imshow(filename, img2)
    cv2.destroyAllWindows()

def test3(allfiles):
    print "entering test3"
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:
        testfiles.append(filetoadd)
    img = []
    img_color = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))
        img_color.append(cv2.imread(testfile, -1))
        img_computed.append(np.zeros_like(img[i]))
        i+=1

    def compute(iimg, color_iimg, bilateral1=10, bilateral2=50, bilateral3=120, th1=19, th2=255,a=1,b=1):
        #if bilateral2%2 == 0 or bilateral1%2 == 0:
            #return iimg
        print bilateral1 , bilateral2, bilateral3, th1, th2
       # oimg = cv2.fastNlMeansDenoising(iimg)
        clahe= cv2.createCLAHE(clipLimit=3, tileGridSize=(197,197))
        oimg = clahe.apply(iimg)
        oimg = cv2.bilateralFilter(oimg,bilateral1, bilateral2, bilateral3)
        #oimg = an.anisodiff(iimg,bilateral1, bilateral2, bilateral3/10,(a,b))
        #oimg = cv2.GaussianBlur(oimg,(bilateral1, bilateral2),bilateral3)
        #thres, oimg = cv2.threshold(oimg,th1,th2,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        #oimg = cv2.adaptiveThreshold(oimg,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,bilateral1,bilateral2)
        blurred = np.copy(oimg)
        oimg = cv2.Canny(oimg,th1,th2)
        #oimg2 = np.zeros_like(iimg)
        #oimg3 = np.zeros_like(iimg)
#
 
        size = 430
        h = iimg.shape[1] 
        w = iimg.shape[0]

        nSize = 50
        bg = np.zeros_like(iimg)
        #szukam miejsc nalezacych do otoczenia
        xstart=0
        xstop=0
        ystart=0
        ystop=0
        for x in range(0,w):
            for y in range(0, h):
                if x - nSize <0:    xstart = 0
                else:               xstart = x - nSize
                if x + nSize >w:    xstop  = w
                else:               xstop  = x + nSize

                if y - nSize <0:    ystart = 0
                else:               ystart = y - nSize
                if y + nSize >h:    ystop  = h
                else:               ystop  = y + nSize
                if cv2.countNonZero(oimg[xstart:xstop+1, ystart:ystop+1]) >0:
                    bg[x,y] = 255
                    #bg[x,y] = 0
                else: 
                    bg[x,y] = 0
                    #bg[x,y] = 128

        #bg = cv2.threshold(bg, 1,128,1)
        iw_max = int(w/size)
        ih_max = int(h/size)
        xstop = 0
        ystop = 0
        for x in range(1,iw_max+1): 
            if x == iw_max:     xstop = w+1
            else:               xstop = x*size
            for y in range(1,ih_max+1):
                if y == ih_max:     ystop = h+1
                else:               ystop = y*size
                ret, blurred[(x-1)*size:xstop, (y-1)*size:ystop] = cv2.threshold(blurred[(x-1)*size:xstop, (y-1)*size:ystop],th1,th2,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                #thres, oimg2[(x-1+0.5)*size:xstop+0.5*size, (y-1+0.5)*size:ystop+0.5*size] = cv2.threshold(oimg[(x-1+0.5)*size:xstop+0.5*size, (y-1+0.5)*size:ystop+0.5*size],th1,th2,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                #oimg[(x-1)*size:xstop, (y-1)*size:ystop]= cv2.adaptiveThreshold(oimg[(x-1)*size:xstop, (y-1)*size:ystop],255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, th1,th2)
        #oimg = cv2.bitwise_not(cv2.bitwise_and(cv2.bitwise_not(oimg2),cv2.bitwise_not(oimg3)))

        blurred = cv2.bitwise_not(blurred)
        kernel = np.ones((3,3),'uint8')
        blurred = cv2.erode(blurred, kernel, iterations=2)


        marker = cv2.add(blurred, bg)
        marker32 = np.int32(marker)
        cv2.watershed(color_iimg,marker32)
        ret = cv2.convertScaleAbs(marker32)
        
        ret = cv2.inpaint(iimg,bg,3,cv2.INPAINT_TELEA)
        return ret

    name = 'test3' 
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)

    #trackbars
    cv2.createTrackbar('d',name, 10, 255, nothing)
    cv2.createTrackbar('sigmaColor',name, 50, 255, nothing)
    cv2.createTrackbar('sigmaSpace',name, 90, 255, nothing) 
    cv2.createTrackbar('th1',name, 19, 255, nothing) 
    cv2.createTrackbar('th2',name, 255, 255, nothing) 
    cv2.createTrackbar('a',name, 1, 31, nothing) 
    cv2.createTrackbar('b',name, 1, 31, nothing) 

    cv2.resizeWindow(name, 1024, 768)
    
    while(1):
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
        if k == ord(' '):
            a = cv2.getTrackbarPos('d',name)
            b = cv2.getTrackbarPos('sigmaColor',name)
            c = cv2.getTrackbarPos('sigmaSpace',name)
            d = cv2.getTrackbarPos('th1',name)
            e = cv2.getTrackbarPos('th2',name)
            f = cv2.getTrackbarPos('a',name)
            g = cv2.getTrackbarPos('b',name)
            
            i = 0
            for srcimg in img[0:4]:
                img_computed[i] = compute(srcimg,img_color[i],a,b,c,d,e,f,g)
                i+=1
            finalimg = make4img(0.6, img_computed[0], img_computed[1], img_computed[2], img_computed[3])
            cv2.imwrite('test.tiff', finalimg)
            cv2.imshow(name, finalimg)
    cv2.destroyAllWindows()

def test4(allfiles):
    print "entering test4"
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:
        testfiles.append(filetoadd)
    img = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, -1))
        #img_computed.append(np.zeros_like(img[i,:,0]))
        i+=1

    hsl = []
    i = 0
    for x in img:
        hsl.append(cv2.cvtColor(img[i], cv2.COLOR_BGR2HLS))
        i+=1

    l = []
    for x in hsl:
        l.append(x[:,:,0])

    l2 = []
    for x in l:
        l2.append(cv2.Canny(x,254,255))
      
    combined = make4img(0.6, l2[0], l2[1], l2[2], l2[3])
    cv2.imshow('image', combined)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


    img = []
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))

def test5(allfiles):
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:
        testfiles.append(filetoadd)
    img = []
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))
    img2 = []
    #equ = cv2.equalizeHist(img[1])
    #equ = an.anisodiff(equ,5,90,0.1,(5,5))
    equ = cv2.bilateralFilter(img[1],16,3,88)
    size = 300
    h = img[1].shape[1] 
    w = img[1].shape[0]
    
    iw_max = int(w/size)
    ih_max = int(h/size)
    xstop = 0
    ystop = 0
    for x in range(1,iw_max+1): 
        if x == iw_max:     xstop = w+1
        else:               xstop = x*size
        for y in range(1,ih_max+1):
            if y == ih_max:     ystop = h+1
            else:               ystop = y*size
            thres, equ[(x-1)*size:xstop, (y-1)*size:ystop] = cv2.threshold(equ[(x-1)*size:xstop, (y-1)*size:ystop],19,255,cv2.THRESH_TOZERO + cv2.THRESH_OTSU)
            #equ[(x-1)*size:xstop, (y-1)*size:ystop] = cv2.adaptiveThreshold(equ[(x-1)*size:xstop, (y-1)*size:ystop],255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 191,8)
    #res = np.hstack((img[1],equ)) #stacking images side-by-side

    #for x in img:
        #img2.append(an.anisodiff(x,5,90,0.1,(5,5)))
    #combined = make4img(0.6,img2[0], img2[1], img2[2], img2[3])

    cv2.imshow('image', equ)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test6(allfiles):
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:
        testfiles.append(filetoadd)
    img = []
    img_color = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))
        img_color.append(cv2.imread(testfile, -1))
        img_computed.append(np.zeros_like(img[i]))
        i+=1

    def compute(iimg, color_iimg, bilateral1=10, bilateral2=50, bilateral3=120, th1=19, th2=255,):
        print bilateral1 , bilateral2, bilateral3, th1, th2
        clahe= cv2.createCLAHE(clipLimit=3, tileGridSize=(197,197))
        oimg = clahe.apply(iimg)
        oimg = cv2.bilateralFilter(oimg,bilateral1, bilateral2, bilateral3)
        #oimg = cv2.GaussianBlur(oimg,(bilateral1, bilateral2),bilateral3)
        #thres, oimg = cv2.threshold(oimg,th1,th2,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        #oimg = cv2.adaptiveThreshold(oimg,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,bilateral1,bilateral2)
        blurred = np.copy(oimg)
        oimg = cv2.Canny(oimg,66,1)
#
 
        size = 430
        h = iimg.shape[1] 
        w = iimg.shape[0]

        nSize = 50
        bg = np.zeros_like(iimg)
        #szukam miejsc nalezacych do otoczenia
        xstart=0
        xstop=0
        ystart=0
        ystop=0
        for x in range(0,w):
            for y in range(0, h):
                if x - nSize <0:    xstart = 0
                else:               xstart = x - nSize
                if x + nSize >w:    xstop  = w
                else:               xstop  = x + nSize
#
                if y - nSize <0:    ystart = 0
                else:               ystart = y - nSize
                if y + nSize >h:    ystop  = h
                else:               ystop  = y + nSize
                if cv2.countNonZero(oimg[xstart:xstop+1, ystart:ystop+1]) >0:
                    bg[x,y] = 255
                    #bg[x,y] = 0
                else: 
                    bg[x,y] = 0
                    #bg[x,y] = 128
        
        ret = cv2.inpaint(iimg,bg,3,cv2.INPAINT_NS)
        ret = cv2.divide(bg,iimg)

        return ret



    name = 'test6' 
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    #trackbars
    cv2.createTrackbar('d',name, 10, 255, nothing)
    cv2.createTrackbar('sigmaColor',name, 50, 255, nothing)
    cv2.createTrackbar('sigmaSpace',name, 90, 255, nothing) 
    cv2.createTrackbar('th1',name, 19, 255, nothing) 
    cv2.createTrackbar('th2',name, 255, 255, nothing) 

    cv2.resizeWindow(name, 1024, 768)
    
    while(1):
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
        if k == ord(' '):
            a = cv2.getTrackbarPos('d',name)
            b = cv2.getTrackbarPos('sigmaColor',name)
            c = cv2.getTrackbarPos('sigmaSpace',name)
            d = cv2.getTrackbarPos('th1',name)
            e = cv2.getTrackbarPos('th2',name)
            
            i = 0
            for srcimg in img[0:4]:
                img_computed[i] = compute(srcimg,img_color[i],a,b,c,d,e)
                i+=1
            finalimg = make4img(0.6, img_computed[0], img_computed[1], img_computed[2], img_computed[3])
            cv2.imwrite('test.tiff', finalimg)
            cv2.imshow(name, finalimg)
    cv2.destroyAllWindows()

    
def test7(allfiles):
    print 'test7'
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[1:14]:
        testfiles.append(filetoadd)
    img = []
    img_color = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))
        img_color.append(cv2.imread(testfile, -1))
        
    k_small = cv2.getStructuringElement(cv2.cv.CV_SHAPE_ELLIPSE, (3,3))
    k_medium = cv2.getStructuringElement(cv2.cv.CV_SHAPE_ELLIPSE, (17,17))
    iterimg = 0
    
    iterator = 0
    for i in img:
        colorSrc = img_color[iterator]
        iterator+=1
        i = cv2.pyrDown(i)

###
        #clahe= cv2.createCLAHE(clipLimit=2, tileGridSize=(3,3))
        #i = clahe.apply(i)
#
        #i = my.sharpenImage(i)
        #i = my.sharpenImage(i)
#
        #i = my.denoise(i)
        
###
        max_value = 0;
        x =-1 # not used
        (_, max_value,_,_) = cv2.minMaxLoc(i)
        i = np.multiply(i , (float(255)/max_value))
        i = i.astype(np.uint8)
        base = np.copy(i)

        i = cv2.adaptiveThreshold(i,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,25,-5)
        th = np.copy(i)
        #img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 61,9)
        i = cv2.dilate(i,k_medium, anchor=(-1,-1), iterations=2)
        dil1 = np.copy(i)

        i = cv2.inpaint(base,dil1,3,cv2.INPAINT_TELEA)
        background = np.copy(i)
        i = cv2.absdiff(base, background)
        diff = np.copy(i)

        (_, i) = cv2.threshold(diff,10,255,cv2.THRESH_BINARY)
        i = cv2.dilate(i,k_small)
        dil2 = np.copy(i)

        (contours,_) = cv2.findContours(i,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(i,contours, -1,255,cv2.cv.CV_FILLED)
        contours1 = np.copy(i)

        i = cv2.inpaint(base, i, 3, cv2.INPAINT_TELEA)
        diff2 = cv2.absdiff(base, i)

        clahe= cv2.createCLAHE(clipLimit=4, tileGridSize=(9,9))
        diff2 = clahe.apply(diff2)

        ### Canny try

        diff2 = my.denoise(diff2)
        blurred = cv2.GaussianBlur(diff2,(5,5),7)
        binaryImg = cv2.Canny(blurred,50,90)

        # detect circles in the image
        #blurred = cv2.GaussianBlur(diff2,(3,3),7)
        #(_,height) = blurred.shape
        #circles = cv2.HoughCircles(blurred, cv2.cv.CV_HOUGH_GRADIENT, 1,1, param1=110,param2=40,minRadius=0,maxRadius=height/6)
     #
        ## ensure at least some circles were found
        #if circles is not None:
            ## convert the (x, y) coordinates and radius of the circles to integers
            #circles = np.round(circles[0, :]).astype("int")
     #
            ## loop over the (x, y) coordinates and radius of the circles
            #for (x, y, r) in circles:
                ## draw the circle in the output image, then draw a rectangle
                ## corresponding to the center of the circle
                #cv2.circle(blurred, (x, y), r, (255, 255, 255), 4)
                #cv2.rectangle(blurred, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
         #
            ## show the output image
            #cv2.imshow("output", blurred)
            #cv2.waitKey(0)
        
        #############

        ###
        # threshold 0/1-255 -> 0/255

        h = diff2.shape[1] 
        w = diff2.shape[0]
        
        #tempfg = np.zeros_like(diff2)
#
        #for x in range(0,w):
            #for y in range(0, h):
                #if diff2[x,y] > 50:
                    #tempfg[x,y] = 255

        ###

        ####
        #clahe= cv2.createCLAHE(clipLimit=3, tileGridSize=(20,20))
        #i = clahe.apply(diff2)
        ####
        

        ### OTSU TRY
        #size = 90
        #h = diff2.shape[1] 
#
        #w = diff2.shape[0]#
        #iw_max = int(w/size)
        #ih_max = int(h/size)
        #xstop = 0
        #ystop = 0
        #for x in range(1,iw_max+1): 
            #if x == iw_max:     xstop = w+1
            #else:               xstop = x*size
            #for y in range(1,ih_max+1):
                #if y == ih_max:     ystop = h+1
                #else:               ystop = y*size
                #(_, diff2[(x-1)*size:xstop, (y-1)*size:ystop]) = cv2.threshold(diff2[(x-1)*size:xstop, (y-1)*size:ystop],0,255,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        #(_,diff2) = cv2.threshold(diff2, 70,255,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        ###


        #################################################################################################################

        #bimg= cv2.adaptiveThreshold(diff2,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,25,-5)
        #i = cv2.Canny(diff2,1,100)

        #x, bimg = cv2.threshold(diff2, 90,255,cv2.THRESH_BINARY)
        #img_computed.append(base)
        #img_computed.append(th)
        #img_computed.append(dil2)
        #img_computaed.append(background)
        #img_computed.append(diff)
        #img_computed.append(contours1)

        ##
        #(_,binaryImg) = cv2.threshold(diff2,10,254,cv2.THRESH_BINARY)
        #kernel = np.ones((7,7),np.uint8)
        #openedImg = cv2.morphologyEx(binaryImg,cv2.MORPH_OPEN,kernel)
        
        #(contours,_)= cv2.findContours(openedImg,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE) 
        contours, hierarchy= cv2.findContours(binaryImg,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 

        (contours,bad_c) = my.removeTrashContours(contours, w/10)
        blank = np.ones_like(binaryImg)
        cv2.drawContours(blank,contours,-1,255,thickness=3)
        cv2.drawContours(blank,bad_c,-1,120,thickness=1)

        ## show the output image
        cv2.imshow("output", blank)
        #cv2.imshow("output", binaryImg)
        cv2.waitKey(0)
        #img_computed.append(binaryImg)
        #img_computed.append(cannyImg)
        ##

        #img_computed.append(tempfg)
        #img_computed.append(i)
        #img_computed.append(cont_to_show)
        #img_computed.append(circles_to_draw)

    #to_show = make4img(1,img_computed[0],img_computed[1],img_computed[2],img_computed[3])
    #cv2.imshow('a',to_show )

    my.showAllImages(img_computed)
    

def denoiseTest(allfiles):
    print 'denoise test ...'

    #wybranie czesci obrazow
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:

        testfiles.append(filetoadd)
    img = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))

    #zmniejszenie
    imgs_smaller = my.pyrDownArr(img)

    #rozjasnienie 
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    imgs_brighter = []

    for i in imgs_smaller: 
        i = my.sharpenImage(i)
        i =  cv2.bilateralFilter(i,10,50,120)
        imgs_brighter.append( my.scaleLum(i))
        #imgs_brighter.append( clahe.apply(i))

    #denoise
    for i in imgs_brighter:
        img_computed.append(my.denoise(i))

    my.showAllImages(img_computed)

def sharpenTest(allfiles):
    print 'sharpen test ...'

    #wybranie czesci obrazow
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:
        testfiles.append(filetoadd)
    img = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))

    #zmniejszenie
    imgs_smaller = my.pyrDownArr(img)

    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    img_computed = []
    for i in imgs_smaller:
        i = my.sharpenImage(i)
        i = my.sharpenImage(i)
        i = my.sharpenImage(i)
        i = my.sharpenImage(i)
        i = my.denoise(i)
        i = clahe.apply(i)
        i = my.scaleLum(i)
        #i =  cv2.bilateralFilter(i,10,50,120)
        #img_computed.append(my.sharpenImage(i))

       # i = cv2.adaptiveThreshold(i,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,25,-5)
        
        i = cv2.Canny(i,10,30)

        
        img_computed.append(i)

        


    my.showAllImages(img_computed)
    
def testEM(allfiles):
    print 'EM TEST'
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[10:14]:
        testfiles.append(filetoadd)
    img = []
    img_color = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))
        img_color.append(cv2.imread(testfile, -1))


def test8(allfiles):
    print 'test7'
    testfiles = []
    testfiles.append(allfiles[0])
    for filetoadd in allfiles[1:14]:
        testfiles.append(filetoadd)
    img = []
    img_color = []
    img_computed = []
    i=0
    for testfile in testfiles:
        img.append(cv2.imread(testfile, 0))
        img_color.append(cv2.imread(testfile, -1))
    img_computed = []
    for i in img:
        i = cv2.pyrDown(i)
        i = cv2.Sobel(i,cv2.CV_32F,3,3, ksize=5, scale = 0.05)
        img_computed.append(i)
    my.showAllImages(img_computed)
##########################################################################################
#getting courrent path
path = os.getcwd()
print "searching for file to compute in: ", path

#getting all the files in directory
allfiles = [ f for f in os.listdir(path) if (isfile(join(path, f)) & f.endswith(".tiff")) ]
allfiles.sort(key=natural_keys)

#printing found files
print "found files: \n"
for instance in allfiles:
    print instance 

###
#test1(allfiles)
#test2(allfiles)
#test3(allfiles) #parameter checking
#test4(allfiles) #hsl testing
#test5(allfiles)
#test6(allfiles)
test7(allfiles)
#test8(allfiles)
#denoiseTest(allfiles)
#sharpenTest(allfiles)
